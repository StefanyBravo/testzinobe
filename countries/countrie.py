# encoding: utf-8
import requests

import pandas as pd
import json
import hashlib
import os

#conexion sqlite
from conexion import SqlConexion


class Region:
    
    def get_region(self):
        url = 'https://restcountries-v1.p.rapidapi.com/all'

        headers = {            
            'x-rapidapi-key': 'a72dbed86bmsh8b86a67f4f202d3p1d90bajsndaabfc29c6d1',
            'x-rapidapi-host': 'restcountries-v1.p.rapidapi.com',
            'useQueryString': 'true'
        }
        response = requests.request("GET", url, headers=headers)
        if response.status_code == 200:
            df_response = pd.read_json(response.text)
            region = df_response['region'].unique()
            region = region[:6]
            region.sort()
            return region


class Countries:

    def get_country(self,region_name):
        url = 'https://restcountries.eu/rest/v2/region/'+region_name
        response = requests.request("GET", url)
        if response.status_code == 200:
            data = json.loads(response.text) 
            time = response.elapsed.microseconds/1000 
            country_name = data[0]['name']
            crypto = hashlib.sha1(data[0]['languages'][0]['name'].encode()).hexdigest()
            return country_name, crypto, time
        return 0,0,0


class CreateDataFrame:
        
    def data_frame(self):
        region = Region().get_region()
        city_name = []
        language = []
        _time = []

        for i in region:            
            c_name, list_language, time = Countries().get_country(region_name=i)
            city_name.append(c_name)
            language.append(list_language)  
            _time.append(time)            

        df = pd.DataFrame(
            {'Region': region, 'City Name': city_name, 'Languaje': language,'Time': _time})
        
        return df


class InsertDataFrame:
    
    def insert_data(self,df):        
        json_df = df.to_json(orient='records')
        SqlConexion().sql_insert(json_df)
    

class PrintDataFrame:
    
    def print_data_frame(self,df):
        print('-'*80)
        print(df)
        print('-'*80)
        print("--- Times ----")
        #print(df.describe())
        print("Tiempo total:",(df.sum().Time))
        print("Tiempo promedio:",df.mean().Time)
        print("Tiempo minimo:",df.min().Time)
        print("Tiempo maximo:",df.max().Time)

        
df =  CreateDataFrame().data_frame()       
InsertDataFrame().insert_data(df=df)
PrintDataFrame().print_data_frame(df=df)