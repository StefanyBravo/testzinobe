# encoding: utf-8
import unittest
from conexion import SqlConexion
import json

class TestCases(unittest.TestCase):

    def test_sql_lite_json(self,):
        test = {'city': 'colombia', 'languaje': 'español', 'region': 'america'}
        obj=SqlConexion().sql_insert(json.dumps(test))
        self.assertIsNone(obj)
        
    def test_sql_lite_dict(self,):
        test_dict = {'city': 'colombia', 'languaje': 'español', 'region': 'america'}
        obj_dict=SqlConexion().sql_insert(test_dict)
        self.assertIsNone(obj_dict)

if __name__ == '__main__':
    unittest.main()
