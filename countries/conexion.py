import sqlite3
from sqlite3 import Error


class SqlConexion:

    def main(self):
        self.sql_connection()

    def sql_connection(self,):
        try:
            con = sqlite3.connect('region.db')
            return con
        except Error:
            print(Error)

    def sql_table(self, con):
        cursorObj = con.cursor()
        cursorObj.execute(
            "CREATE TABLE IF NOT EXISTS  region(id integer PRIMARY KEY  AUTOINCREMENT, json_value json_text,created_at TEXT DEFAULT CURRENT_TIMESTAMP)")
        con.commit()

    def sql_insert(self, data):
        con = self.sql_connection()
        self.sql_table(con)
        cursorObj = con.cursor()
        cursorObj.execute(
            'INSERT INTO region(json_value) VALUES(?)', (data,))
        con.commit()
